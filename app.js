"use strict";

let webSocketsServerPort = 8080;
let webSocketServer = require('websocket').server;
let http = require('http');
let util = require('util');

//Managers
let roomManager = require('./roomManager.js');
let userManager = require('./userManager.js');

//HTTP server
let server = http.createServer(function(request, response) {})
server.listen(webSocketsServerPort, function() {
	log('Socket is running on port ' + webSocketsServerPort)
});

//WebSocket server
let ws = new webSocketServer({
	httpServer: server
});

//onOpen
ws.on('request', function(request) {
	let connection = request.accept(null, request.origin)

	//Active user
	let userName = request.httpRequest.headers.login
	let currentUser = new userManager.User(userName, connection)

	log(currentUser.name + ' has joined the server')
	userManager.addNewUser(currentUser)

	//onMessage
	connection.on('message', function(message) {
		if (message.type === 'utf8') {
			let request = JSON.parse(message.utf8Data)

			if (request.type == 'register') {
					connection.sendUTF(generateJoinResp(request.id, currentUser))
					return
			}

			if (request.type == 'room_list') {
					log('room_list was requested');
					connection.sendUTF(generateRoomResp(request.id))
					return
			}

			if (request.type == 'user_list') {
					log('user_list was requested');
					userManager.getUser(users => {
						connection.sendUTF(generateUserResp(request.id, users))
					});
					return
			}

			if (request.type == 'room_join') {
					console.log("Attempting to add user -> " + currentUser.name	);
					let result = roomManager.addUserToRoom(currentUser, request.room_name)
					let room = roomManager.getRoomByName(request.room_name)

					console.log("Join: Room found? -> " + room.name);
					if (!room && !room.name) {
						console.log("Join: Room not found!");
						return
					}

					room.broadcastMessage(generateRoomJoinResp(request.id, result))
					console.log(room);
					return
			}

			if (request.type == 'text_message') {
					let room = roomManager.getRoomByName(request.room_name)
					if (!room && !room.name) {
						console.log("Room not found!");
						return
					}
					console.log("Room found! -> " + room.name + "\n Room users: " + room.users.length);
					room.broadcastMessage(generateMessage(-1, request.message, userName))
					return
			}
		}
	})

	//onClose
	connection.on('close', function(connection) {
		//TODO notify user manager about closing the connection
		log(userName + " has left the server")
	})
})

function generateUserResp(id, users) {
			return JSON.stringify({
					id: id,
					message: JSON.stringify(users),
					content: JSON.stringify({
						users: users
					}),
					command: 'user_list'
					// message: "Rooms available: " + roomManager.getRooms().length
			})}

//Response containing all roooms
function generateRoomResp(id) {
			return JSON.stringify({
					id: id,
					message: JSON.stringify(roomManager.getRooms()[0]),
					content: JSON.stringify({
						rooms: roomManager.getRooms()
					}),
					command: 'room_list'
					// message: "Rooms available: " + roomManager.getRooms().length
			})}

//Chat join (user has registered)
function generateJoinResp(id, user) {
			return JSON.stringify({
					id: id,
					message: "User " + user.name + " has joined the chat",
					content: JSON.stringify({
						user_id: user.id,
						user_name: user.name,
						users_list: util.inspect(userManager.getUsers(), { depth: null })
					}),
					command: 'join_notice'})
			}

//Room join (User has joined the room)
function generateRoomJoinResp(id, resultJson) {
			return JSON.stringify({
					id: id,
					message: JSON.parse(resultJson).resultMsg,
					content: resultJson,
					command: 'room_join'})
			}

//Text message
function generateMessage(id, message, senderName) {
			return JSON.stringify({
					id: id,
					message: message,
					content: JSON.stringify({
						timestamp: Date.now(),
						sender: senderName,
						text: message
					}),
					command: 'text_message'})
			}

function log (message) {
			console.log((new Date()) + ' -> ' + message)
}
