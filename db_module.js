'use strict';

let mongo = require('mongodb');
require('promise');

const dbUrl = 'mongodb://localhost:27017/';
const dbName = 'chat_db';
const dbTableName = 'users';

module.exports = {
  getUsersFromDb: (callback) => requestUsers(callback),
  addUser: (user) => addUserInner(user)
}

//DEBUG method
requestUsers(result => console.log('From callback: \n %o', result));

// var user = {
//   name: "Anon100",
//   test: true
// };
// addUserInner(user, callback => {
//   console.log("Inserted ?");
// });

function addUserInner(user, callback) {
  mongo.connect(dbUrl, (err, db) => {
      if (err) {
        console.log("Connecion to database : %s has failed!", dbUrl);
        callback("Failed to add user", 1);
        if (db) db.close();
        return;
      }
      console.log("Connected to database : %s", dbUrl);
      let dbo = db.db(dbName);

      accessOrCreate(dbo, dbTableName)
         .then(insertUser(user, dbo.collection(dbTableName))
          .then(() => {
            callback("User was added", 0)
          }))
         .then(db.close());

  });
}

function insertUser(user, collection) {
  return new Promise((resolve, reject) => {
    try {
      collection.insert(user);
    } catch (err) {
      throw err;
    }
    resolve();
  });
}

function requestUsers(callback) {
  mongo.connect(dbUrl, (err, db) => {
      if (err) {
        console.log("Connecion to database : %s has failed!", dbUrl);
        callback(err)
        if (db) db.close();
        return;
      }
      console.log("Connected to database : %s", dbUrl);
      let dbo = db.db(dbName);

      accessOrStop(dbo, dbTableName).catch(() => {}) //ignore promise reject
        .then(queryTable(dbo, dbTableName)
          .then(result => {
            if (result.length == 0) {
              callback ([]);
              return; //secure the case if response is empty
            }
            if (callback) callback (result);
            else console.log("No callback in query");
          }))
        .then(db.close());
  });
}

function queryTable(dbo, tableName) {
  return new Promise(resolve =>
    dbo.collection(tableName)
      .find()
      .project({ _id: 0 })
      .toArray((err, result) => {
        if (err) throw err;
        resolve(result);
      }));
}

function accessOrCreate(dbo, tableName) {
  return checkIfExists(dbo, tableName).then(result => {
    return new Promise(resolve => {
      if (result) {
        resolve();
      } else {
        dbo.createCollection(dbTableName, (err, res) => {
            if (err) throw err;
            console.log("Table created : %s", dbTableName);
          }).then(resolve());
      }
    });
  })
}

function accessOrStop(dbo, tableName) {
  return checkIfExists(dbo, tableName).then(result => {
    return new Promise((resolve, reject) => {
      result ? resolve() : reject();
    });
  });
}

function checkIfExists(dbo, table) {
  return dbo.listCollections().toArray().then(collections => {
    return collections.map(item => item.name).includes(table);
  })
}
