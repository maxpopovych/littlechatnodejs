"use strict";

let userManager = require('./userManager.js');
require('websocket')

let rooms = new Map();

let counter = 0

rooms.set("Room 1", new Room("Room 1"))
rooms.set("Room 2", new Room("Room 2"))
rooms.set("Room 3", new Room("Room 3"))

module.exports = {
  getRooms: () => Array.from(rooms.values()).map(room => new RoomJson(room).toJson()),
  getRoomByName: (name) => rooms.get(name),
  addNewRoom: (name) => rooms.set(name, new Room(name)),
  addUserToRoom: (user, room) => addToRoom(user, room),
  Room
}

function addToRoom(user, room) {
  if (rooms.has(room)) {
      console.log("Room found! Attempting to add user " + user.name + " to room -> " + room);
      rooms.get(room).users.push(user)
      console.log("Now users at room -> " + rooms.get(room).users);
      return JSON.stringify({
        resultCode: 0,
        resultMsg: "User " + user.name + " joined " + room
      })
  } else {
    console.log("No room was found " + room);
      return JSON.stringify({
        resultCode: 404,
        resultMsg: "No room was found " + room
      })
  }
}

function Room(name) {
  this.id = ++counter
  this.name = name;
  this.users = [ ];
  this.maxAllowed = 5
}

function RoomJson(room) {
    this.id = room.id
    this.name = room.name
    this.users = room.users.map(user => user.name)
    this.maxAllowed = room.maxAllowed
}

Room.prototype.broadcastMessage = function(msg) {
  console.log("Attempting to broadcastMessage for room: " + this.name + " -> " + msg);
    for (let i in this.users) {
      let user = this.users[i]
      user.connection.sendUTF(msg)
    }
}

RoomJson.prototype.toJson = function() {
  return JSON.stringify({
    id: this.id,
    name: this.name,
    usersCount: this.users.length,
    maxAllowed: this.maxAllowed
  })
}

Room.prototype.addMember = function (user) {
  this.users.push(user);
};
