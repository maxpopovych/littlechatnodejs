"use strict";

let util = require('util');
let dbManager = require('./db_module');

let users = new Map();
let counter = 0;

module.exports = {
  getUsers: () => Array.from(users.values()).map(user => new UserJson(user).toJson()),
  getUserByName: (name) => users.get(name),
  addNewUser: (user) => {
    dbManager.addUserInner(user, callback => {
      console.log("Inserted into db:" + user.name);
    })
    users.set(user.name, user)
  },
  User,
  UserJson
}

function addUser(user) {
  users.set(user.name, user)
  dbManager.insertUser(user)
}

function User(name, connection) {
  this.id = ++counter
  this.name = name
  this.connection = connection
}

function UserJson(user) {
  this.id = user.id
  this.name = user.name
  this.online = user.connection ? user.connection.connected : false
}

UserJson.prototype.toJson = function() {
  return JSON.stringify({
    id: this.id,
    name: this.name,
    online: this.online
  })
}

User.prototype.setName = function (name) {
  this.name = name
};

User.prototype.setConnection = function (connection) {
  this.connection = connection
};
